//
//  PromoViewController.swift
//  NewAppTwo
//
//  Created by user151787 on 9/25/19.
//  Copyright © 2019 user151787. All rights reserved.
//

import UIKit
import GoogleSignIn

class PromoViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var promoCodeTextField: UITextField!
    
    @IBOutlet weak var nameLabel: UILabel!
 //   @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    
    @IBOutlet weak var but1: UIButton!
    
    
    @IBOutlet weak var but2: UIButton!
    
    
    @IBOutlet weak var but3: UIButton!
    
    @IBOutlet weak var but4: UIButton!
    
    
    @IBOutlet weak var but5: UIButton!
    
    @IBOutlet weak var but6: UIButton!
    
    @IBOutlet weak var but7: UIButton!
    
    @IBOutlet weak var but8: UIButton!
    
    @IBOutlet weak var but9: UIButton!
    
    @IBOutlet weak var but10: UIButton!
    
    
    var useremail = ""
    var username = ""
override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = UIColor.white;
    
    promoCodeTextField.backgroundColor = UIColor.white;
    nameLabel.backgroundColor = UIColor.white;
    
    promoCodeTextField.delegate = self
    
    promoCodeTextField.tintColor = UIColor.black;
    
    
    let serviceUrl = "https://soleamexicangrill.com/memApp.php?user=\(useremail)";
    let url = URL(string: serviceUrl)
    
    if let url = url{
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { (data, response, error) in
            if error == nil {
                self.parseJson(data:data!)
            }
            else {
                print("Try again later")
            }
        }
        task.resume()
    }
}
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        promoCodeTextField.resignFirstResponder()
        
        return true
        
        
    }
    func parseJson(data: Data){
        
        do{
            let jsonArray = try JSONSerialization.jsonObject(with: data, options: []) as! [Any]
            for jsonResult in jsonArray{
                let jsonDict = jsonResult as! [String:AnyObject]
                    DispatchQueue.main.async {
                        
                        
                        let bdnum = jsonDict["pnum"]
                        let cont  = jsonDict["cont"]
                        if (cont as! String != "" ) {
                            
                        self.emailLabel.text = self.useremail
                            //self.nameLabel.text = "Welcome "+self.username+(cont! as! String);
                            
                            self.nameLabel.text = (cont! as! String);
                        }
                        else {
                           // self.emailLabel.text = self.useremail
                            self.nameLabel.text = "Welcome "+self.username+(cont! as! String);
                        }
                        if(bdnum! as! String == "1") {
                                                   
                                                   
                                                   self.but1.isSelected = true
                                                   
                                                   }
                                               
                                               if(bdnum! as! String == "2"){
                                                   
                                                   
                                                  
                                                   
                                                   self.but1.isSelected = true
                                                   
                                                   
                                                   
                                                   self.but2.isSelected = true
                                                   
                                                   
                                                   
                                                   
                                               }
                                               
                                               if(bdnum! as! String == "3") {
                                                   self.but1.isSelected = true
                                                   
                                                   
                                                   
                                                   self.but2.isSelected = true
                                                   
                                                   self.but3.isSelected = true
                                                   
                                               }
                                       if(bdnum! as! String == "4") {
                                           self.but1.isSelected = true
                                           
                                           
                                           
                                           self.but2.isSelected = true
                                           
                                           self.but3.isSelected = true
                                           
                                           self.but4.isSelected = true
                                           
                                       }
                                       if(bdnum! as! String == "5") {
                                           self.but1.isSelected = true
                                           
                                           
                                           
                                           self.but2.isSelected = true
                                           
                                           self.but3.isSelected = true
                                           
                                           self.but4.isSelected = true
                                           
                                           self.but5.isSelected = true
                                          
                                       }
                                       
                                       if(bdnum! as! String == "6") {
                                           self.but1.isSelected = true
                                           
                                           
                                           
                                           self.but2.isSelected = true
                                           
                                           self.but3.isSelected = true
                                           
                                           self.but4.isSelected = true
                                           
                                           self.but5.isSelected = true
                                           self.but6.isSelected = true
                                          
                                          
                                       }
                                       if(bdnum! as! String == "7") {
                                           self.but1.isSelected = true
                                           
                                           
                                           
                                           self.but2.isSelected = true
                                           
                                           self.but3.isSelected = true
                                           
                                           self.but4.isSelected = true
                                           
                                           self.but5.isSelected = true
                                           self.but6.isSelected = true
                                           self.but7.isSelected = true
                                           
                                       }
                                       if(bdnum! as! String == "8") {
                                           
                                          self.but1.isSelected = true
                                           
                                           
                                           
                                           self.but2.isSelected = true
                                           
                                           self.but3.isSelected = true
                                           
                                           self.but4.isSelected = true
                                           
                                           self.but5.isSelected = true
                                           self.but6.isSelected = true
                                           self.but7.isSelected = true
                                           self.but8.isSelected = true
                                           
                                       }
                                       if(bdnum! as! String == "9") {
                                           
                                   
                                        
                                           self.but1.isSelected = true
                                           self.but2.isSelected = true
                                           
                                           self.but3.isSelected = true
                                           
                                           self.but4.isSelected = true
                                           
                                           self.but5.isSelected = true
                                           self.but6.isSelected = true
                                           self.but7.isSelected = true
                                           self.but8.isSelected = true
                                           self.but9.isSelected = true
                                           
                        
                                       }
                                       if(bdnum! as! String == "10") {
                                                          
                                                  
                                                       
                                                          self.but1.isSelected = true
                                                          self.but2.isSelected = true
                                                          
                                                          self.but3.isSelected = true
                                                          
                                                          self.but4.isSelected = true
                                                          
                                                          self.but5.isSelected = true
                                                          self.but6.isSelected = true
                                                          self.but7.isSelected = true
                                                          self.but8.isSelected = true
                                                          self.but9.isSelected = true
                                                          self.but10.isSelected = true
                                       
                                                      }
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                               if jsonDict["pnum"]! as! String == "0" {
                                                   
                                                   self.but1.isSelected = false
                                                   self.but2.isSelected = false
                                                   
                                                   self.but3.isSelected = false
                                                   
                                                   self.but4.isSelected = false
                                                   
                                                   self.but5.isSelected = false
                                                   self.but6.isSelected = false
                                                   self.but7.isSelected = false
                                                   self.but8.isSelected = false
                                                   self.but9.isSelected = false
                                                   self.but10.isSelected = false
                                               }
                }
            }
                
                  
                     
            
        }
        catch {
            print("There is an error... try again later")
        }
        
    }
    
    func showToast(controller:UIViewController,message:String,seconds:Double){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title:"Ok",style:.default)
        alert.addAction(okAction)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha=0.6
        alert.view.layer.cornerRadius = 15
        controller.present(alert,animated:true)
        DispatchQueue.main.asyncAfter(deadline:DispatchTime.now()+seconds){
            alert.dismiss(animated: true)
        }
        
    }
    
let URL_SAVE_PROMO = "https://soleamexicangrill.com/upMemApp.php"
    
    
    
    @IBAction func savePromo(_ sender: UIButton) {
        
        
        //created NSURL
        let requestURL = NSURL(string: URL_SAVE_PROMO)
        
        //creating NSMutableURLRequest
        let request = NSMutableURLRequest(url: requestURL! as URL)
        
        //setting the method to post
        request.httpMethod = "POST"
        
        //getting values from text fields
       
        var pnum = ""
        let promoCode = promoCodeTextField.text
        if(promoCode == "q981"){
             pnum = "1"
            self.but1.isSelected = true
            
           
        }
        else if(promoCode == "w872"){
             pnum = "2"
            self.but2.isSelected = true
            
        }
        else if(promoCode == "e763"){
             pnum = "3"
            self.but3.isSelected = true
            
        }
        else if(promoCode == "r654"){
             pnum = "4"
            self.but4.isSelected = true
        }
        else if(promoCode == "t545"){
             pnum = "5"
            self.but5.isSelected = true
        }
        else if(promoCode == "t436"){
             pnum = "6"
            self.but6.isSelected = true
        }
        else if(promoCode == "u327"){
             pnum = "7"
            self.but7.isSelected = true
        }
        else if(promoCode == "t218"){
             pnum = "8"
            self.but8.isSelected = true
        }
        else if(promoCode == "y109"){
             pnum = "9"
            self.but9.isSelected = true
            
            
        }
        else if(promoCode == "congrats"){
             pnum = "10"
            
            self.but10.isSelected = true
            
        }
        else if(promoCode == "clear10"){
             pnum = "0"
            
            self.but1.isSelected = false
            self.but2.isSelected = false
            
            self.but3.isSelected = false
            
            self.but4.isSelected = false
            
            self.but5.isSelected = false
            self.but6.isSelected = false
            self.but7.isSelected = false
            
            self.but8.isSelected = false
            self.but9.isSelected = false
            self.but10.isSelected = false
            
        }
        else{
            
            self.showToast(controller: self, message: "Please enter valid promo code!!", seconds: 5)
            return;
            
        }
        
        //creating the post parameter by concatenating the keys and values from text field
        let postParameters = "user="+useremail+"&pnum="+pnum;
        //adding the parameters to request body
        request.httpBody = postParameters.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest){
            data, response, error in
            
            if error != nil{
                print("error is \(String(describing: error))")
                return;
            }
            
            //parsing the response
            do {
                //converting resonse to NSDictionary
                let myJSON =  try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                
                //parsing the json
                if let parseJSON = myJSON {
                    
                    //creating a string
                    var msg : String!
                    
                    //getting the json response
                    msg = parseJSON["message"] as! String?
                    
                    //printing the response
                    print(msg!)
                    
                
                    
                }
            } catch {
                print(error)
            }
            
        }
        //executing the task
        task.resume()
        
        
    }
    @IBAction func butun1Tapped(_ sender: UIButton){
        if sender.isSelected{
            sender.isSelected=true
        }else{
            sender.isSelected=false
        }
        
    }
    @IBAction func butun2Tapped(_ sender: UIButton){
        if sender.isSelected{
            sender.isSelected=true
        }else{
            sender.isSelected=false
        }
        
    }
    @IBAction func butun3Tapped(_ sender: UIButton){
        if sender.isSelected{
            sender.isSelected=true
        }else{
            sender.isSelected=false
        }
        
    }
    @IBAction func butun4Tapped(_ sender: UIButton){
        if sender.isSelected{
            sender.isSelected=true
        }else{
            sender.isSelected=false
        }
        
    }
    @IBAction func butun5Tapped(_ sender: UIButton){
        if sender.isSelected{
            sender.isSelected=true
        }else{
            sender.isSelected=false
        }
        
    }
    @IBAction func butun6Tapped(_ sender: UIButton){
        if sender.isSelected{
            sender.isSelected=true
        }else{
            sender.isSelected=false
        }
        
    }
    @IBAction func butun7Tapped(_ sender: UIButton){
        if sender.isSelected{
            sender.isSelected=true
        }else{
            sender.isSelected=false
        }
        
    }
    @IBAction func butun8Tapped(_ sender: UIButton){
        if sender.isSelected{
            sender.isSelected=true
        }else{
            sender.isSelected=false
        }
        
    }
    @IBAction func butun9Tapped(_ sender: UIButton){
        if sender.isSelected{
            sender.isSelected=true
        }else{
            sender.isSelected=false
        }
        
    }
    @IBAction func butun10Tapped(_ sender: UIButton){
        if sender.isSelected{
            sender.isSelected=true
        }else{
            sender.isSelected=false
        }
        
    }
    
    @IBAction func SignOut(_ sender: Any) {
        GIDSignIn.sharedInstance()?.signOut()
        self.dismiss(animated: true, completion: nil)
    }
    
}

