//
//  AdminViewController.swift
//  NewAppTwo
//
//  Created by user151787 on 6/13/19.
//  Copyright © 2019 user151787. All rights reserved.
//

import UIKit



class AdminViewController: UIViewController {
    
    
    let URL_SAVE_TEAM = "https://soleamexicangrill.com/lightsApp.php"
    
    //TextFields declarations
    
    @IBOutlet weak var textFieldGreenContent: UITextField!
    
    @IBOutlet weak var textFieldYellowContent: UITextField!
    
    @IBOutlet weak var textFieldRedContent: UITextField!
    @IBOutlet weak var greenContent: UITextField!
    @IBOutlet weak var green: UITextField!
    @IBOutlet weak var yellow: UITextField!
    @IBOutlet weak var red: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let signoutbutton = UIButton.init(type:.roundedRect)
        signoutbutton.frame = CGRect(x: 100, y: 600, width: 200, height: 50)
        signoutbutton.backgroundColor = .black
        signoutbutton.tintColor = .white
        signoutbutton.titleLabel?.font=UIFont(name:"Arial",size:26)
        signoutbutton.setTitle("Signout", for: .normal)
        signoutbutton.addTarget(self, action: #selector(buttonClicked(_ :)),for: .touchUpInside)
        self.view.addSubview(signoutbutton)
        // Do any additional setup after loading the view.
        textFieldRedContent.delegate = self;
        textFieldGreenContent.delegate = self;
        textFieldYellowContent.delegate = self;
        textFieldYellowContent.tintColor = UIColor.black;
        textFieldRedContent.tintColor = UIColor.black;
        textFieldGreenContent.tintColor = UIColor.black;
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
   /* func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldRedContent.resignFirstResponder()
        
        textFieldYellowContent.resignFirstResponder()
        
        textFieldGreenContent.resignFirstResponder()
        
        return true
        
        
    }*/
    
    
    @objc func buttonClicked(_ : UIButton){
        self.dismiss(animated: true, completion: nil)
        
    }
    func showToast(controller:UIViewController,message:String,seconds:Double){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title:"Ok",style:.default)
        alert.addAction(okAction)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha=0.6
        alert.view.layer.cornerRadius = 15
        controller.present(alert,animated:true)
        DispatchQueue.main.asyncAfter(deadline:DispatchTime.now()+seconds){
            alert.dismiss(animated: true)
        }
        
    }
    
    
    
    @IBAction func buttonSave(_ sender: UIButton) {
        
        
        //created NSURL
        let requestURL = NSURL(string: URL_SAVE_TEAM)
        
        //creating NSMutableURLRequest
        let request = NSMutableURLRequest(url: requestURL! as URL)
        
        //setting the method to post
        request.httpMethod = "POST"
        
        //getting values from text fields
        let green = "true"
        let yellow = "false"
        let red = "false"
        let greencontent = textFieldGreenContent.text
        let yellowcontent = textFieldYellowContent.text
        let redcontent = textFieldRedContent.text
        if (textFieldGreenContent.text != "") {
        //creating the post parameter by concatenating the keys and values from text field
        let postParameters = "green="+green+"&yellow="+yellow+"&red="+red+"&greenContent="+greencontent!+"&yellowContent="+yellowcontent!+"&redContent="+redcontent!;
        //adding the parameters to request body
        request.httpBody = postParameters.data(using: String.Encoding.utf8)
        //request.HTTPBody = postParameters.data(usingEncoding:NSUTF8StringEncoding)
        
        //creating a task to send the post request
        let task = URLSession.shared.dataTask(with: request as URLRequest){
            data, response, error in
            
            if error != nil{
                print("error is \(String(describing: error))")
                return;
            }
            
            //parsing the response
            do {
                //converting resonse to NSDictionary
                let myJSON =  try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                
                //parsing the json
                if let parseJSON = myJSON {
                    
                    //creating a string
                    var msg : String!
                    
                    //getting the json response
                    msg = parseJSON["message"] as! String?
                    
                    //printing the response
                    print(msg!)
                    
                
                    
                }
            } catch {
                print(error)
            }
            
        }
        //executing the task
        task.resume()
        self.showToast(controller: self, message: "Successfully Updated", seconds: 5)
        }
        else {
            self.showToast(controller: self, message: "Please enter events promo!!", seconds: 5)
            
        }
        
    }
   
    
     
     @IBAction func buttonSaveYellow(_ sender: UIButton) {
     
        let requestURL = NSURL(string: URL_SAVE_TEAM)
        
        //creating NSMutableURLRequest
        let request = NSMutableURLRequest(url: requestURL! as URL)
        
        //setting the method to post
        request.httpMethod = "POST"
        
        //getting values from text fields
        let green = "false"
        let yellow = "false"
        let red = "true"
        let greencontent = textFieldGreenContent.text
        let yellowcontent = textFieldRedContent.text
        let redcontent = textFieldYellowContent.text
        if (textFieldYellowContent.text != "") {
        //creating the post parameter by concatenating the keys and values from text field
        let postParameters = "green="+green+"&yellow="+yellow+"&red="+red+"&greenContent="+greencontent!+"&yellowContent="+yellowcontent!+"&redContent="+redcontent!;
        //adding the parameters to request body
        request.httpBody = postParameters.data(using: String.Encoding.utf8)
        //request.HTTPBody = postParameters.data(usingEncoding:NSUTF8StringEncoding)
        
        //creating a task to send the post request
        let task = URLSession.shared.dataTask(with: request as URLRequest){
            data, response, error in
            
            if error != nil{
                print("error is \(String(describing: error))")
                return;
            }
            
            //parsing the response
            do {
                //converting resonse to NSDictionary
                let myJSON =  try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                
                //parsing the json
                if let parseJSON = myJSON {
                    
                    //creating a string
                    var msg : String!
                    
                    //getting the json response
                    msg = parseJSON["message"] as! String?
                    
                    //printing the response
                    print(msg!)
                    
                    
                    
                }
            } catch {
                print(error)
            }
            
        }
        //executing the task
        task.resume()
        self.showToast(controller: self, message: "Successfully Updated", seconds: 5)
        }
        else {
            self.showToast(controller: self, message: "Please enter drinks promo!!", seconds: 5)
            
        }
     }
     
     
     
    @IBAction func buttonSaveRed(_ sender: UIButton) {
        let requestURL = NSURL(string: URL_SAVE_TEAM)
        
        //creating NSMutableURLRequest
        let request = NSMutableURLRequest(url: requestURL! as URL)
        
        //setting the method to post
        request.httpMethod = "POST"
        
        //getting values from text fields
        let green = "false"
        let yellow = "true"
        let red = "false"
        let greencontent = textFieldGreenContent.text
        let yellowcontent = textFieldRedContent.text
        let redcontent = textFieldYellowContent.text
        if (textFieldRedContent.text != "") {
        //creating the post parameter by concatenating the keys and values from text field
        let postParameters = "green="+green+"&yellow="+yellow+"&red="+red+"&greenContent="+greencontent!+"&yellowContent="+yellowcontent!+"&redContent="+redcontent!;
        //adding the parameters to request body
        request.httpBody = postParameters.data(using: String.Encoding.utf8)
        //request.HTTPBody = postParameters.data(usingEncoding:NSUTF8StringEncoding)
        
        //creating a task to send the post request
        let task = URLSession.shared.dataTask(with: request as URLRequest){
            data, response, error in
            
            if error != nil{
                print("error is \(String(describing: error))")
                return;
            }
            
            //parsing the response
            do {
                //converting resonse to NSDictionary
                let myJSON =  try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                
                //parsing the json
                if let parseJSON = myJSON {
                    
                    //creating a string
                    var msg : String!
                    
                    //getting the json response
                    msg = parseJSON["message"] as! String?
                    
                    //printing the response
                    print(msg!)
                    
                    
                    
                }
            } catch {
                print(error)
            }
            
        }
        //executing the task
        task.resume()
        self.showToast(controller: self, message: "Successfully Updated", seconds: 5)
        }
        else {
            self.showToast(controller: self, message: "Please enter food promo!!", seconds: 5)
            
        }
    }
    
     
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    
}
extension AdminViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        
        return true
        
        
    }
    
}
