//
//  LoginViewController.swift
//  NewAppTwo
//
//  Created by user151787 on 6/12/19.
//  Copyright © 2019 user151787. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var userPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backgroundImage = UIImageView(frame:UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "soleabg.jpg")
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
        //userNameTextField.delegate = self
        //userPasswordTextField.delegate = self
        userNameTextField.tintColor = UIColor.black;
        userPasswordTextField.tintColor = UIColor.black;
        // Do any additional setup after loading the view.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func showToast(controller:UIViewController,message:String,seconds:Double){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title:"Ok",style:.default)
        alert.addAction(okAction)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha=0.6
        alert.view.layer.cornerRadius = 15
        controller.present(alert,animated:true)
        DispatchQueue.main.asyncAfter(deadline:DispatchTime.now()+seconds){
            alert.dismiss(animated: true)
        }
        
    }
    
    @IBAction func LoginButtonTapped(_ sender: Any) {
        let userName = userNameTextField.text;
        let userPassword = userPasswordTextField.text;
        let userNameStored = "soleaapp";
        let userPasswordTextField = "soleaapp!@";
        if(userName == ""){
            self.showToast(controller: self, message: "Please enter username!!", seconds: 5)
        }
        else if(userPassword == ""){
            self.showToast(controller: self, message: "Please enter password!!", seconds: 5)
        }
        else if(userName != userNameStored || userPasswordTextField != userPassword) {
            self.showToast(controller: self, message: "Username and Password do not match", seconds: 5)
        }
        else if(userNameStored == userName && userPasswordTextField == userPassword) {
               
                self.performSegue(withIdentifier: "AdminLogin", sender: self);
                
                
        }
      
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LoginViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        
        return true
        
        
    }
    
}
