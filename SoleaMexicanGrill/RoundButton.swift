//
//  RoundButton.swift
//  NewAppTwo
//
//  Created by user151787 on 6/3/19.
//  Copyright © 2019 user151787. All rights reserved.
//

import UIKit
@IBDesignable
class RoundButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat=0{
        didSet{
            self.layer.cornerRadius=cornerRadius 
        }
    }
    @IBInspectable var borderWidth: CGFloat=0{
        didSet{
            self.layer.borderWidth=borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor=borderColor.cgColor
        }
    }
    
    
    
}
