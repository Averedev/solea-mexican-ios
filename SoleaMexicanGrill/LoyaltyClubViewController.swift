//
//  LoyaltyClubViewController.swift
//  NewAppTwo
//
//  Created by user151787 on 9/16/19.
//  Copyright © 2019 user151787. All rights reserved.
//


import UIKit
import Firebase
import GoogleSignIn

@available(iOS 13.0, *)
class LoyaltyCardViewController: UIViewController, UIApplicationDelegate, GIDSignInDelegate{
   var email = ""
    var name = ""
    @IBOutlet weak var labelUserEmail : UILabel?
    @IBOutlet weak var textView:UITextView!
    @IBOutlet weak var textViewHC:NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let googleButton = GIDSignInButton()
        googleButton.frame = CGRect(x: 16, y: 286+136, width: view.frame.width - 32, height: 50)
        view.addSubview(googleButton)
            GIDSignIn.sharedInstance().presentingViewController = self
      //  GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance()?.delegate = self
        
        // self.performSegue(withIdentifier: "PromoCode", sender: self)
        
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        print("Successfully Logged Into gmail")
        self.email = user.profile.email!
        self.name = user.profile.name!
        print(email)
       // labelUserEmail.text = email!
       // labelUserEmail?.text = email
        //labelUserEmail?.frame.origin = CGPoint(x:10, y:600)
    
        performSegue(withIdentifier: "PromoCode", sender: self)
       // let VC = (self.storyboard?.instantiateViewController(identifier: "AdminLogin"))!
       // self.present(VC, animated:true, completion:nil)
        
       
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var vc = segue.destination as! PromoViewController
        vc.useremail = self.email
        vc.username = self.name
    }
}
