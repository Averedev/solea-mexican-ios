//
//  HomeViewController.swift
//  NewAppTwo
//
//  Created by user151787 on 6/19/19.
//  Copyright © 2019 user151787. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    
    

    
    
    @IBOutlet weak var newRedButton: UIButton!
    
    @IBOutlet weak var newYellowButton: UIButton!
    
    @IBOutlet weak var newGreenButton: UIButton!
    
    
    @objc func greenButtonClicked(_ sender: UIButton){
        let par = sender.accessibilityHint
        let alert = UIAlertController(title: nil, message: par, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title:"Ok",style:.default)
        alert.addAction(okAction)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha=0.6
        alert.view.layer.cornerRadius = 15
        self.present(alert,animated:true, completion: nil)
      //  DispatchQueue.main.asyncAfter(deadline:DispatchTime.now()+5){
      //      alert.dismiss(animated: true)
      //  }
        
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
                 let backgroundImage = UIImageView(frame:UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "soleabg.jpg")
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
               // Do any additional setup after loading the view.
        //newRedButton.frame = CGRect(x:163,y:199,width:100,height:100);
        /*newRedButton.layer.cornerRadius = newRedButton.frame.width / 2;
        newRedButton.layer.masksToBounds = true;
        newGreenButton.layer.cornerRadius = newGreenButton.frame.width / 2;
        newGreenButton.layer.masksToBounds = true;
        newYellowButton.layer.cornerRadius = newYellowButton.frame.width / 2;
        newYellowButton.layer.masksToBounds = true;*/
        let serviceUrl = "https://soleamexicangrill.com/retLightsApp.php"
        let url = URL(string: serviceUrl)
        if let url = url{
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { (data, response, error) in
                if error == nil {
                    self.parseJson(data:data!)
                }
                else {
                    // ERROR OCCURRED
                }
            }
            task.resume()
            
        }
    }
    
    
    func parseJson(data: Data){
        
        
        do{
            
            let jsonArray = try JSONSerialization.jsonObject(with: data, options: []) as! [Any]
            for jsonResult in jsonArray{
                let jsonDict = jsonResult as! [String:AnyObject]
               
              
                    DispatchQueue.main.async {
                        
                        //self.newGreenButton.isEnabled = false
                    self.newGreenButton.isEnabled = (jsonDict["green"] as! NSString).boolValue
                    self.newYellowButton.isEnabled = (jsonDict["yellow"] as! NSString).boolValue
                    self.newRedButton.isEnabled = (jsonDict["red"] as! NSString).boolValue
                        self.viewDidLoad()
                        self.viewWillAppear(true)
                        if jsonDict["green"]! as! String == "true" {
                            self.newGreenButton.accessibilityHint = (jsonDict["greenContent"] as? String)
                                
                        self.newGreenButton.addTarget(self, action: #selector(HomeViewController.greenButtonClicked(_:)), for: .touchUpInside)
                            self.newYellowButton.setTitleColor(.black, for: .normal)
                            self.newRedButton.setTitleColor(.black, for: .normal)
                            
                            self.newGreenButton.setTitleColor(UIColor(red: 201/255, green: 32/255, blue: 232/255, alpha: 1.0), for: .normal)
                            
                            
                        }
                        if jsonDict["yellow"]! as! String == "true" {
                            self.newYellowButton.accessibilityHint = jsonDict["yellowContent"] as? String
                            self.newYellowButton.addTarget(self, action: #selector(HomeViewController.greenButtonClicked(_:)), for: .touchUpInside)
                           
                            self.newRedButton.setTitleColor(.black, for: .normal)
                            self.newGreenButton.setTitleColor(.black, for: .normal)
                            self.newYellowButton.setTitleColor(UIColor(red: 201/255, green: 32/255, blue: 232/255, alpha: 1.0), for: .normal)
                            
                            
                        }
                        
                        if jsonDict["red"]! as! String == "true" {
                            self.newRedButton.accessibilityHint = jsonDict["redContent"] as? String
                            self.newRedButton.addTarget(self, action: #selector(HomeViewController.greenButtonClicked(_:)), for: .touchUpInside)
                            self.newYellowButton.setTitleColor(.black, for: .normal)
                           
                            self.newGreenButton.setTitleColor(.black, for: .normal)
                            self.newRedButton.setTitleColor(UIColor(red: 201/255, green: 32/255, blue: 232/255, alpha: 1.0), for: .normal)
                            
                        }
                        
                        // self.newGreenButton.addTarget(self, action: Selector(showToast), for: .touchUpInside)
                       // showToast(controller: self, message: jsonDict["greenContent"] as! String, seconds: 5)
                        
                }
                
                func showToast(controller:UIViewController,message:String,seconds:Double){
                    let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                    let okAction = UIAlertAction.init(title:"Ok",style:.default)
                    alert.addAction(okAction)
                    alert.view.backgroundColor = UIColor.black
                    alert.view.alpha=0.6
                    alert.view.layer.cornerRadius = 15
                    controller.present(alert,animated:true)
                    DispatchQueue.main.asyncAfter(deadline:DispatchTime.now()+seconds){
                        alert.dismiss(animated: true)
                    }
                    
                }
               
            }
        }
        catch {
            print("There is an error")
        }
    }
  
}
