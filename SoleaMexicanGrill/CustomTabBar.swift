//
//  CustomTabBar.swift
//  NewAppTwo
//
//  Created by user151787 on 6/4/19.
//  Copyright © 2019 user151787. All rights reserved.
//

import UIKit

class CustomTabBar: UITabBarController {
    var tabBarIteam = UITabBarItem()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.darkGray], for: .normal)
        let selectedImage1 = UIImage(named:"home")?.withRenderingMode(.alwaysOriginal)
        let deSelectedImage1 = UIImage(named:"home")?.withRenderingMode(.alwaysOriginal)
        tabBarIteam = self.tabBar.items![0]
        tabBarIteam.imageInsets = UIEdgeInsets(top: 8,left: 0,bottom: 0,right: 0);
        
        tabBarIteam.image = deSelectedImage1
        tabBarIteam.selectedImage = selectedImage1
        
       
        let selectedImage2 = UIImage(named:"menu")?.withRenderingMode(.alwaysOriginal)
        let deSelectedImage2 = UIImage(named:"menu")?.withRenderingMode(.alwaysOriginal)
        tabBarIteam = self.tabBar.items![1]
        tabBarIteam.imageInsets = UIEdgeInsets(top: 8,left: 0,bottom: 0,right: 0); tabBarIteam.image = deSelectedImage2
        tabBarIteam.selectedImage = selectedImage2
        
        let selectedImage3 = UIImage(named:"loyalty1")?.withRenderingMode(.alwaysOriginal)
        let deSelectedImage3 = UIImage(named:"loyalty1")?.withRenderingMode(.alwaysOriginal)
        tabBarIteam = self.tabBar.items![2]
        tabBarIteam.imageInsets = UIEdgeInsets(top: 8,left: 0,bottom: 0,right: 0);        tabBarIteam.image = deSelectedImage3
        tabBarIteam.selectedImage = selectedImage3
        
        let selectedImage4 = UIImage(named:"profile")?.withRenderingMode(.alwaysOriginal)
        let deSelectedImage4 = UIImage(named:"profile")?.withRenderingMode(.alwaysOriginal)
        tabBarIteam = self.tabBar.items![3]
        tabBarIteam.imageInsets = UIEdgeInsets(top: 8,left: 0,bottom: 0,right: 0);        tabBarIteam.image = deSelectedImage4
        tabBarIteam.selectedImage = selectedImage4
        
        self.selectedIndex=0
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
